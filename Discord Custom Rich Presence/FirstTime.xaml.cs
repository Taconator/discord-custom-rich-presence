﻿
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Discord_Custom_Rich_Presence
{
    /// <summary>
    /// Interaction logic for FirstTime.xaml
    /// </summary>
    public partial class FirstTime
    {
        public FirstTime()
        {
            InitializeComponent();
        }

        private void OpenAppsPage(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://discordapp.com/developers/applications/");
        }

        private void Next_Click(object sender, RoutedEventArgs e)
        {
            if (long.TryParse(ClientIDBox.Text, out long ClientID))
            {
                Properties.Settings.Default.ClientID = ClientID;
                Properties.Settings.Default.Save();
                DialogResult = true;
                Close();
            } else
            {
                ClientIDBox.BorderBrush = Brushes.Red;
                ToolTip TT = new ToolTip
                {
                    Content = "Client ID\nYou must enter a number"
                };
                ClientIDBox.ToolTip = TT;
                TT.IsOpen = true;
            }
        }
    }

}

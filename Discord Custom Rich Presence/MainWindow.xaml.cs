﻿using System;
using System.Windows.Threading;
using System.Windows;
using System.Windows.Media;
using DiscordRPC;
using DiscordRPC.Logging;
using System.Diagnostics;
using System.Windows.Forms;
using System.Windows.Navigation;
using ControlzEx.Standard;
using Application = System.Windows.Application;
using MahApps.Metro;

namespace Discord_Custom_Rich_Presence
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        private DiscordRpcClient Client;
        private DispatcherTimer InvokeTimer = new DispatcherTimer { Interval = new TimeSpan(0, 0, 10) };

        public NotifyIcon TrayIcon = new NotifyIcon
        {
            Text = "Discord Custom Rich Presence",
            Icon = Properties.Resources.AppIcon,
            ContextMenu = new ContextMenu()
        };
        public MainWindow()
        {
            InitializeComponent();
        }

        protected override void OnStateChanged(EventArgs e)
        {
            if (WindowState == WindowState.Minimized)
            {
                Hide();
                TrayIcon.Visible = true;
                if (Properties.Settings.Default.NotifOnMinimize) TrayIcon.ShowBalloonTip(1500, "Discord Custom Rich Presence Now Hidden", "Click the cog icon in the notifications tray to reopen", ToolTipIcon.None);
            }

            base.OnStateChanged(e);
        }

        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            if (Properties.Settings.Default.ClientID == -1)
            {
                FirstTime FT = new FirstTime();
                bool Result = (bool)FT.ShowDialog();
                if (!Result)
                {
                    Application.Current.Shutdown();
                }
            }
            LoadSettings();
            if (Properties.Settings.Default.ActivateOnOpen)
            {
                UpdateRPC();
            }
            Version.Text = "Version " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            InvokeTimer.Tick += InvokeTimer_Tick;
            MenuItem ExitItem = new MenuItem
            {
                Text = "Exit"
            };
            MenuItem Show = new MenuItem
            {
                Text = "Show"
            };
            ExitItem.Click += (s, a) =>
            {
                if (Properties.Settings.Default.ConfirmExit)
                {
                    ConfirmClose CloseDialog = new ConfirmClose();
                    if (!(bool)CloseDialog.ShowDialog())
                    {
                        Hide();
                        TrayIcon.Visible = true;
                    }
                }
                else Application.Current.Shutdown();
            };
            EventHandler ShowHandler = new EventHandler(delegate (object s, EventArgs a)
            {
                this.Show();
                WindowState = WindowState.Normal;
                Activate();
                TrayIcon.Visible = false;
            });
            Show.Click += ShowHandler;
            TrayIcon.Click += ShowHandler;
            TrayIcon.ContextMenu.MenuItems.AddRange(new MenuItem[] { Show, ExitItem });
        }

        private void ToggleSettings(object sender, RoutedEventArgs e)
        {
            SettingsFlyout.IsOpen = !SettingsFlyout.IsOpen;
        }

        private void ToggleAbout(object sender, RoutedEventArgs e)
        {
            AboutFlyout.IsOpen = !AboutFlyout.IsOpen;
        }

        private void ToggleHelp(object sender, RoutedEventArgs e)
        {
            HelpFlyout.IsOpen = !HelpFlyout.IsOpen;
        }

        private void LoadSettings()
        {
            State.Text = Properties.Settings.Default.State;
            Details.Text = Properties.Settings.Default.Details;
            LargeImage.Text = Properties.Settings.Default.LargeImage;
            LargeImageTip.Text = Properties.Settings.Default.LargeImageTooltip;
            SmallImageKey.Text = Properties.Settings.Default.SmallImage;
            SmallImageTip.Text = Properties.Settings.Default.SmallImageTooltip;
            if (Properties.Settings.Default.StartTimestamp != null) StartTimestamp.SelectedDate = Properties.Settings.Default.StartTimestamp;
            if (Properties.Settings.Default.EndTimestamp != null) EndTimestamp.SelectedDate = Properties.Settings.Default.EndTimestamp;
            ClientID.Text = Properties.Settings.Default.ClientID.ToString();
            RichPresenceToggle.IsChecked = Properties.Settings.Default.ActivateOnOpen;
            MinimizeNotif.IsChecked = Properties.Settings.Default.NotifOnMinimize;
            ConfirmExit.IsChecked = Properties.Settings.Default.ConfirmExit;
            DarkTheme.IsChecked = Properties.Settings.Default.DarkTheme;
            if (Properties.Settings.Default.DarkTheme)
            {
                ThemeManager.ChangeAppTheme(Application.Current, "BaseDark");
            } else
            {
                ThemeManager.ChangeAppTheme(Application.Current, "BaseLight");
            }
            ReloadItems();
        }

        private void InvokeTimer_Tick(object sender, EventArgs e)
        {
            Client.Invoke();
        }

        private void ReloadItems()
        {
            LargeImage.Items.Clear();
            SmallImageKey.Items.Clear();
            foreach (String CachedImage in Properties.Settings.Default.LargeImages)
            {
                LargeImage.Items.Add(CachedImage);
            }
            foreach (string CachedImage in Properties.Settings.Default.SmallImages)
            {
                SmallImageKey.Items.Add(CachedImage);
            }
        }

        private static void OnPresenceUpdate(object sender, DiscordRPC.Message.PresenceMessage Message)
        {
            System.Windows.Forms.MessageBox.Show(Message.ToString());
        }

        private Tuple<bool, string> UpdateRPC()
        {
            if (Client == null || Client.ApplicationID != Properties.Settings.Default.ClientID.ToString())
            {
                Client = new DiscordRpcClient(Properties.Settings.Default.ClientID.ToString())
                {
                    Logger = new ConsoleLogger {Level = LogLevel.Warning}
                };
                Client.OnPresenceUpdate += OnPresenceUpdate;
                Client.Initialize();
            }
            if (Properties.Settings.Default.State == "") return new Tuple<bool, string>(false, "State");
            if (Properties.Settings.Default.LargeImage == "") return new Tuple<bool, string>(false, "Large image key");
            if (Properties.Settings.Default.Details == "") return new Tuple<bool, string>(false, "Details");
            RichPresence RP = new RichPresence
            {
                State = Properties.Settings.Default.State,
                Details = Properties.Settings.Default.Details,
                /*Timestamps = new Timestamps
                {
                    Start = Properties.Settings.Default.StartTimestamp,
                    End = Properties.Settings.Default.EndTimestamp
                },*/
                Assets = new Assets()
                {
                    LargeImageKey = Properties.Settings.Default.LargeImage,
                    LargeImageText = Properties.Settings.Default.LargeImageTooltip,
                    SmallImageKey = Properties.Settings.Default.SmallImage,
                    SmallImageText = Properties.Settings.Default.SmallImageTooltip
                }
            };
            Client.SetPresence(RP);
            return new Tuple<bool, string>(true, "Success");
        }

        private void Apply_Click(object sender, RoutedEventArgs e)
        {
            if (Details.Text == "")
            {
                Details.BorderBrush = Brushes.Red;
                Details.ToolTip = "Can not be empty";
                ErrorText.Text = "Details is required and thus cannot be nothing";
                ErrorFlyout.IsOpen = true;
                return;
            }
            else {
                Details.BorderBrush = new SolidColorBrush(Color.FromRgb(128, 128, 128));
                Details.ToolTip = "Text on top";
            }
            if (State.Text == "")
            {
                State.BorderBrush = Brushes.Red;
                State.ToolTip = "Can not be empty";
                ErrorText.Text = "State is required and thus cannot be nothing";
                ErrorFlyout.IsOpen = true;
                return;
            }
            else
            {
                State.BorderBrush = new SolidColorBrush(Color.FromRgb(128, 128, 128));
                State.ToolTip = "Text on bottom";
            }
            Properties.Settings.Default.Details = Details.Text;
            Properties.Settings.Default.State = State.Text;
            if (StartTimestamp.SelectedDate != null)
            {
                Properties.Settings.Default.StartTimestamp = (DateTime)StartTimestamp.SelectedDate;
            }
            if (EndTimestamp.SelectedDate != null)
            {
                Properties.Settings.Default.EndTimestamp = (DateTime)EndTimestamp.SelectedDate;
            }
            Properties.Settings.Default.LargeImage = LargeImage.Text;
            Properties.Settings.Default.SmallImageTooltip = SmallImageTip.Text;
            Properties.Settings.Default.LargeImageTooltip = LargeImageTip.Text;
            Properties.Settings.Default.SmallImage = SmallImageKey.Text;
            Properties.Settings.Default.Save();
            Tuple<bool, string> Result = UpdateRPC();
            if (!Result.Item1) {
                ErrorText.Text = Result.Item2 + " cannot be nothing, it's required";
                ErrorFlyout.IsOpen = true;
                return;
            }
            if (!Properties.Settings.Default.SmallImages.Contains(SmallImageKey.Text) && SmallImageKey.Text != "") Properties.Settings.Default.SmallImages.Add(SmallImageKey.Text);
            if (!Properties.Settings.Default.LargeImages.Contains(LargeImage.Text) && LargeImage.Text != "") Properties.Settings.Default.LargeImages.Add(LargeImage.Text);
            Properties.Settings.Default.Save();
            ReloadItems();
        }

        private void ApplySettings(object sender, RoutedEventArgs e)
        {
            if (long.TryParse(ClientID.Text, out long Converted))
            {
                Properties.Settings.Default.ClientID = Converted;
                Properties.Settings.Default.ActivateOnOpen = (bool)RichPresenceToggle.IsChecked;
                Properties.Settings.Default.NotifOnMinimize = (bool)MinimizeNotif.IsChecked;
                Properties.Settings.Default.ConfirmExit = (bool)ConfirmExit.IsChecked;
                Properties.Settings.Default.DarkTheme = (bool)DarkTheme.IsChecked;
                Properties.Settings.Default.Save();
                if (Properties.Settings.Default.DarkTheme)
                {
                    ThemeManager.ChangeAppTheme(Application.Current, "BaseDark");
                }
                else
                {
                    ThemeManager.ChangeAppTheme(Application.Current, "BaseLight");
                }
                SettingsFlyout.IsOpen = false;
            }
            else
            {
                ClientID.BorderBrush = Brushes.Red;
                ClientID.ToolTip = "Client ID must be a whole number";
            }
        }

        private void MetroWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (Client != null) Client.Dispose();
        }

        private void OpenAppsPage(object sender, RoutedEventArgs e) => Process.Start("https://discordapp.com/developers/applications/");

        private void OpenMetro(object sender, RoutedEventArgs e) => Process.Start("https://github.com/MahApps/MahApps.Metro");

        private void OpenEWTK(object sender, RoutedEventArgs e) => Process.Start("https://github.com/xceedsoftware/wpftoolkit");

        private void OpenDRPC(object sender, RoutedEventArgs e) => Process.Start("https://github.com/Lachee/discord-rpc-csharp");

        private void OpenTKSB(object sender, RoutedEventArgs e) => Process.Start("https://discord.gg/FXDaawa");
    }
}

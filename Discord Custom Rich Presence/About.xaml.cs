﻿namespace Discord_Custom_Rich_Presence
{
    /// <summary>
    /// Interaction logic for About.xaml
    /// </summary>
    public partial class About
    {
        public About()
        {
            InitializeComponent();
        }

        private void OpenMetro()
        {
            System.Diagnostics.Process.Start("https://github.com/MahApps/MahApps.Metro");
        }

        private void OpenEWT()
        {
            System.Diagnostics.Process.Start("https://github.com/xceedsoftware/wpftoolkit");
        }

        private void OpenDRPC()
        {
            System.Diagnostics.Process.Start("https://github.com/Lachee/discord-rpc-csharp/");
        }
    }
}

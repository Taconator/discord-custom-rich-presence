﻿namespace Discord_Custom_Rich_Presence
{
    /// <summary>
    /// Interaction logic for Settings.xaml
    /// </summary>
    public partial class Settings
    {
        public Settings()
        {
            InitializeComponent();
            ClientID.Text = Properties.Settings.Default.ClientID.ToString();
        }

        private void ApplySettings()
        {
            if (int.TryParse(ClientID.Text, out int Converted))
            {
                Properties.Settings.Default.ClientID = Converted;
                Properties.Settings.Default.ActivateOnOpen = (bool)RichPresence.IsChecked;
                Close();
            } else
            {
                ClientID.BorderBrush = System.Windows.Media.Brushes.Red;
                ClientID.ToolTip = "Client ID must be a whole number";
            }
        }
    }
}

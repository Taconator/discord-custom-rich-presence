﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Discord_Custom_Rich_Presence
{
    /// <summary>
    /// Interaction logic for ConfirmClose.xaml
    /// </summary>
    public partial class ConfirmClose
    {
        public ConfirmClose()
        {
            InitializeComponent();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e) => Close();

        private void Confirm_Click(object sender, RoutedEventArgs e) => Application.Current.Shutdown();
    }
}
